import React, { useState, useEffect } from "react";
import Toggle from "react-toggle";
import "react-toggle/style.css";
import "./style/Navibar.css";
import "./style/CardObject.css";

const Navibar = () => {
  const [isLeft, setIsLeft] = useState(false);
  const [isSmallScreen, setIsSmallScreen] = useState(false);

  useEffect(() => {
    function handleResize() {
      setIsSmallScreen(window.innerWidth <= 1000);
    }

    handleResize();

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
      console.log("произошло размонитрование");
    };
  }, []);

  useEffect(() => {
    // Инициализация значения переменной CSS при загрузке компонента
    document.documentElement.style.setProperty(
      "--text-color",
      "var(--green-color)"
    );
  }, []);

  const toggleToSwitchBgImage = () => {
    setIsLeft(!isLeft);
    const textColorSpaceCard = !isLeft
      ? "var(--white-color)"
      : "var(--green-color)";
    document.documentElement.style.setProperty(
      "--text-color",
      textColorSpaceCard
    );
  };

  useEffect(() => {
    const bodyElement = document.querySelector("body");

    // Добавляем или удаляем классы в зависимости от значения isLeft
    if (!isLeft) {
      bodyElement.classList.add("background-space");
      bodyElement.classList.remove("background-soyuz");
    } else {
      bodyElement.classList.add("background-soyuz");
      bodyElement.classList.remove("background-space");
    }
  }, [isLeft]);
  return (
    <>
      <div className="navbar-container ">
        <div className="navbar-content">
          <div className="logo-comapny-item">
            <img src="asLogo.png" height={60} alt="Ancient Soft" />
          </div>
          <p className="nav-item name-website">
            Калькулятор веса на космических объектах
          </p>
          <div className={isSmallScreen ? "hidden-toggle" : "item-button"}>
            <Toggle
              checked={isLeft}
              icons={false}
              onChange={toggleToSwitchBgImage}
            />
            {console.log("маленький экран ", isSmallScreen)}
            <div className="horizontal-separator" />
            <span className="default-text">Переключить фон</span>
          </div>
        </div>
      </div>
    </>
  );
};

export default Navibar;
