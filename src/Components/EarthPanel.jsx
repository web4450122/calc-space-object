import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./style/EarthPanel.css";
import "../style.css";
import Form from "react-bootstrap/Form";
import InputGroup from "react-bootstrap/InputGroup";
import CardObject from "./CardObject";
import { Planets } from "../assets/Assets.js";

const EarthPanel = () => {
  const images = [
    { src: Planets.sun, title: "Солнце" },
    { src: Planets.mercury, title: "Меркурий" },
    { src: Planets.venus, title: "Венера" },
    { src: Planets.earth, title: "Земля" },
    { src: Planets.moon, title: "Луна" },
    { src: Planets.mars, title: "Марс" },
    { src: Planets.jupiter, title: "Юпитер" },
    { src: Planets.saturn, title: "Сатурн" },
    { src: Planets.uranus, title: "Уран" },
    { src: Planets.neptune, title: "Нептун" },
    { src: Planets.plutone, title: "Плутон" },
  ];

  const gravityFactors = {
    Солнце: 27.9,
    Меркурий: 0.38,
    Венера: 0.91,
    Земля: 1,
    Луна: 0.165,
    Марс: 0.38,
    Юпитер: 2.34,
    Сатурн: 1.06,
    Уран: 0.92,
    Нептун: 1.19,
    Плутон: 0.06,
  };

  const [yourWeight, setYourWeight] = useState();

  const handleChange = (e) => {
    const value = e.target.value;
    if (/^\d*$/.test(value)) {
      setYourWeight(value);
    }
  };

  const handleKeyPress = (e) => {
    if (!/^\d*$/.test(e.key)) {
      e.preventDefault();
    }
  };

  const calculateWeight = (planet) => {
    if (yourWeight) {
      return (yourWeight * gravityFactors[planet]).toFixed(2);
    }
  };

  return (
    <div className="root">
      <div className="earth-input-item">
        <img
          src={images.find((image) => image.title === "Земля").src}
          alt="Земля"
          className="earth-img"
        />
        <InputGroup className="mb-1 itemInputWeight">
          <Form.Control
            placeholder="Введите вес на планете Земля (кг)"
            aria-label=""
            aria-describedby="basic-addon2"
            className="customInput"
            value={yourWeight}
            onChange={handleChange}
            onKeyPress={handleKeyPress}
          />
        </InputGroup>
      </div>
      <div className="planets-box">
        {images
          .filter((image) => image.title !== "Земля")
          .map((image, index) => (
            <CardObject
              key={index}
              img={image.src}
              name={image.title}
              altName={image.title}
              weight={calculateWeight(image.title)}
            />
          ))}
      </div>
    </div>
  );
};

export default EarthPanel;
