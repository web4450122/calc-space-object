export const Planets = {
  sun: require("./images/sun.png"),
  mercury: require("./images/mercury.png"),
  venus: require("./images/venus.png"),
  earth: require("./images/earth.png"),
  moon: require("./images/moon.png"),
  mars: require("./images/mars.png"),
  jupiter: require("./images/jupiter.png"),
  saturn: require("./images/saturn.png"),
  uranus: require("./images/uranus.png"),
  neptune: require("./images/neptune.png"),
  plutone: require("./images/plyton.png"),
};
