import React from "react";
import "./style/CardObject.css";
import "../style.css";

const CardObject = ({ img, name, altName, weight }) => {
  return (
    <div className="card-planet">
      <span className="nameSpaceObject textColor">{name}</span>
      <img src={img} alt={altName} className="planet-image-size" />
      <div className="fieldOutDataWeight">{weight ? `${weight} кг` : ""}</div>
    </div>
  );
};

export default CardObject;
