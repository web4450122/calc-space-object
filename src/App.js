import React from "react";
import Navibar from "./Components/Navibar";
import EarthPanel from "./Components/EarthPanel";
import Footer from "./Components/Footer";
function App() {
  return (
    <>
      <Navibar />
      <EarthPanel />
      <Footer />
    </>
  );
}

export default App;
