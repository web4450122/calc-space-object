import React from "react";
import "./style/Footer.css";

const Footer = () => {
  return (
    <footer className="custom-footer">
      <p className="txt-footer">
        <a
          className="footer-link"
          href="https://gitlab.com/web4450122/calc-space-object"
          target="blank"
        >
          &copy; 2024&nbsp; Ancient Soft.
        </a>{" "}
      </p>
    </footer>
  );
};

export default Footer;
